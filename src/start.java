public class start {

	public static void main(String[] args) {
		
	
		Person p1 = new Teacher("Andrea","Gelsomino",'M',1,"Android");
		p1.sign();
		((Teacher) p1).assigHomeworks();
		
		Person p2 = new Student("Carolina","Ferrer",'F',2);
		p2.sign();
		((Student) p2).makesHomeworks();
		
		Person p3 = new Director("Josep","Perez",'F',3);
		p3.sign();
		((Director) p3).payTeachers();
		
		
		//check the instance
		if (p1 instanceof Teacher) {
			System.out.println(p1.name +  " " +  p1.surname +  " is a teacher");			
		}
		if (p1 instanceof Student) {
			System.out.println(p1.name +  " " +  p1.surname +  " is a Student");			
		}
		if (p1 instanceof Director) {
			System.out.println(p1.name +  " " +  p1.surname +  " is a Director");			
		}
		
		
	}
	

}
