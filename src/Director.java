public class Director extends Person {
	
	public Director(String name, String surname, char gender, int id) {
		super(id,name, surname,gender);
	}
	
	public void payTeachers() {
			System.out.println("Director: " +  this.name + " need to pay teachers");
	}
		
	public void sign() {
		System.out.println("Director: " +  this.name + " is signing");
	}

}
