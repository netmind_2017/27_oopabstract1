public class Student extends Person {
	
	public Student(String name, String surname, char gender, int id) {
		super(id,name, surname,gender);
	}
	
	public void sign() {
			System.out.println("Stutent: " +  this.name + " is sign");
	}
	
	public void makesHomeworks() {
		System.out.println("Student: " +  this.name + " makes homeworks");
	}
	
}

