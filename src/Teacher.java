public class Teacher extends Person {
	
	public String courseName;
	
	public Teacher(String name, String surname, char gender, int id, String courseName) {	
		super(id,name, surname,gender);
		this.courseName = courseName;
	}
	
	public void sign() {
		System.out.println("Teacher: " +  this.name + " is sign");
	}
	
	public void assigHomeworks() {
		System.out.println("Teacher: " +  this.name + " is assigning homework");
	}
	
}
