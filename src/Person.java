//abstract class
public abstract class Person {
	
	private Integer id;
	public String name;
	public String surname;
	public char gender;
		
	public Person(Integer id, String name, String surname,char gender){
		this.id=id;
		this.name=name;
		this.surname=surname;
		this.gender=gender;		
	}
	
	//sign attendance
	public abstract void sign();
	

		
}	
